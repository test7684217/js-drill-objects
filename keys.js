exports.keys = (object) => {
  try {

    if(typeof object!='object'){
        console.log("Input is not an object")
        return [];
    }

    let result = [];

    for (const key in object) {
      result.push(key);
    }

    return result;
  } catch (error) {
    console.log(error);
  }
};
