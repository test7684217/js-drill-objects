exports.pairs = (object) => {
  try {
    if (typeof object != "object") {
      console.log("Input is not an object");
      return {};
    }

    if (typeof object !== "object") {
      return [];
    }

    let result = [];

    for (const key in object) {
      let pair = [key, object[key]];

      result.push(pair);
    }

    return result;
  } catch (error) {
    console.log(error);
  }
};
