exports.defaults = (object, defaultProps) => {
  try {
    if (typeof object != "object") {
      console.log("Input is not an object");
      return {};
    }
    for (const key in defaultProps) {
      if (!object[key]) {
        object[key] = defaultProps[key];
      }
    }

    return object;
  } catch (error) {
    console.log(error);
  }
};
