exports.invert = (object) => {
  try {
    if (typeof object != "object") {
      console.log("Input is not an object");
      return {};
    }
    let result = {};

    for (const key in object) {
      let newKey = object[key];
      let newValue = key;

      result[newKey] = newValue;
    }

    return result;
  } catch (error) {
    console.log(error);
  }
};
