exports.values = (object) => {
  try {
    if (typeof object != "object") {
      console.log("Input is not an object");
      return [];
    }
    let result = [];

    for (const key in object) {
      result.push(object[key]);
    }

    return result;
  } catch (error) {
    console.log(error);
  }
};
