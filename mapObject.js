exports.mapObject = (object, cb) => {
  try {
    if (typeof object != "object") {
      console.log("Input is not an object");
      return {};
    }

    let newObject = {};

    if (typeof object !== "object") {
      return {};
    }

    for (const key in object) {
      const newValue = cb(object[key]);
      newObject[key] = newValue;
    }

    return newObject;
  } catch (error) {
    console.log(error);
  }
};
